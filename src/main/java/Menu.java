import java.util.*;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Locale locale;
    ResourceBundle bundle;

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("Q", bundle.getString("Q"));
    }

    public Menu() {
        locale = new Locale("uk");
        locale = new Locale("de");

        locale = new Locale("es");


        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::UkrainianMenu);
        methodsMenu.put("2", this::EnglishMenu);
        methodsMenu.put("3", this::GermanyMenu);
        methodsMenu.put("4", this::SpainMenu);


    }

    private void UkrainianMenu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void EnglishMenu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void GermanyMenu() {
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }
    private void SpainMenu() {
        locale = new Locale("es");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }





    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
